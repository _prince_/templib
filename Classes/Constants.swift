//
//  Constants.swift
//  Pods-TempLib_Example
//
//  Created by Manpreet Singh on 09/08/18.
//


import UIKit


class Constants {
    
    
    static var IS_SIMULATOR: Bool {
        
        return TARGET_OS_SIMULATOR != 0
    }
}

