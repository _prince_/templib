# TempLib

[![CI Status](https://img.shields.io/travis/Manpreet Singh/TempLib.svg?style=flat)](https://travis-ci.org/Manpreet Singh/TempLib)
[![Version](https://img.shields.io/cocoapods/v/TempLib.svg?style=flat)](https://cocoapods.org/pods/TempLib)
[![License](https://img.shields.io/cocoapods/l/TempLib.svg?style=flat)](https://cocoapods.org/pods/TempLib)
[![Platform](https://img.shields.io/cocoapods/p/TempLib.svg?style=flat)](https://cocoapods.org/pods/TempLib)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TempLib is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TempLib'
```

## Author

Manpreet Singh, prince6179@gmail.com

## License

TempLib is available under the MIT license. See the LICENSE file for more info.
