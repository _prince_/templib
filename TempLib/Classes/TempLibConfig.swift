//
//  TempLibConfig.swift
//  Pods-TempLib_Example
//
//  Created by Manpreet Singh on 15/08/18.
//


import UIKit


struct TempLibConfig {
    
    
    var accountID: String
    
    var amount: Double
    
    var currency: String
    
    var customerBillingAddress1: String
    
    var customerBillingAddress2: String     // Optional
    
    var customerBillingCity: String
    
    var customerBillingCompany: String      // Optional
    
    var customerBillingCountry: String
    
    var customerBillingPhone: String
    
    var customerBillingState: String
    
    var customerBillingZip: String
    
    var customerEmail: String
    
    var customerFirstName: String
    
    var customerLastName: String
    
    var customerPhone: String               // Optional
    
    var customerShippingAddress1: String
    
    var customerShippingAddress2: String    // Optional
    
    var customerShippingCity: String
    
    var customerShippingCompany: String     // Optional
    
    var customerShippingCountry: String
    
    var customerShippingFirstName: String
    
    var customerShippingLastName: String
    
    var customerShippingPhone: String
    
    var customerShippingState: String
    
    var customerShippingZip: String
    
    var description: String                 // Optional
    
    var invoice: String                     // Optional
    
    var planID: String                      // Optional
    
    var reference: String
    
    var shopCountry: String
    
    var shopName: String
    
    // signature        // test
    
    var urlCallback: URL
    
    var urlCancel: URL
    
    var urlComplete: URL
    
    
    /*static let sharedInstance = TempLibConfig()
    
    
    private static func sharedInstanceWith(accountID: String,
                                           amount: Float,
                                           currency: String) -> TempLibConfig {
        
        let instance = TempLibConfig.sharedInstance
        
        
        instance.userName = userName
        
        instance.email = email
        
        instance.userID = userID
        
        
        return instance
    }
    
    
    private init(accountID:                 String = "",
                 amount:                    Double = 0.0,
                 currency:                  String = "",
                 customerBillingAddress1:   String = "",
                 customerBillingAddress2:   String = "") {
        
        self.accountID                  = accountID
        
        self.amount                     = amount
        
        self.currency                   = currency
        
        self.customerBillingAddress1    = customerBillingAddress1
        
        self.customerBillingAddress2    = customerBillingAddress2
    }*/
}

