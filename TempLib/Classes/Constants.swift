//
//  Constants.swift
//  Pods-TempLib_Example
//
//  Created by Manpreet Singh on 09/08/18.
//


import UIKit


class Constants {
    
    
    static let LIVE_URL = "https://api.paybright.com"
    
    static let TEST_URL = "https://sandbox.paybright.com"
    
    
    static var IS_SIMULATOR: Bool {
        
        return TARGET_OS_SIMULATOR != 0
    }
    
    
    var IS_TEST_MODE: Bool = false
    
    
    var API_KEY: String = ""
    
    
    var API_TOKEN: String = ""
}

